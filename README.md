# My Journey to the Core

## What is this about?

This repository is sort of a _blog_ that I decided to create using plain-old
[Markdown](https://spec.commonmark.org/), and to publish as a simple
[Git](https://git-scm.com/) repository.

The idea with the blog is to document my adventures along the path I walk every
single day as a computer scientist. The articles and blogs I read, the tools I
know, the choices I make, the knowledge I gain.

Hopefully, **you** at the other side of the screen, can join me in my
adventures and become better programmers together. I'm sure you will discover
something you didn't know minutes ago at the least.

## About me

I'm Edinson E. Padrón Urdaneta, a very curious and passionate computer
scientist and software engineer who is always looking into new technologies,
patterns, methodologies, and ideas in order to expand his ever-growing toolkit
and improve as a profesional on the most dynamic and fascinating journey I've
ever had the pleasure of being part of.

## Blog mode

For a better reading experience, I wrote a tiny [CSS style
sheet](./assets/stylus/blog-mode.css) you can enable in your web browser with
the help of [Stylus](https://github.com/openstyles/stylus). Here is a preview:
![Blog mode activated for a better reading experience](./assets/images/blog-mode-preview.png)

## Index

- [Blogs, articles, talks, conferences and other useful resources](./resources/)
- [Tutorials, how-tos an more on software development](./software-development/)
- [Tutorials, how-tos an more on cloud computing](./cloud-computing/)
- [Quality assurance](./quality-assurance/)
- [My GNU/Linux adventures](./linux/my-gnu-linux-adventures.md)
- [To The Core - The YT Channel](./to-the-core.md)
- [Talks](./talks.md)
