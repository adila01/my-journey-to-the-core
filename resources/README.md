# Resources

## About

This section contains each and every resource I've had the pleasure to
encounter in my path and that has made me a better programmer, or at very least
taught me something.


### Blogs, articles, and any other written media

#### Cloud computing
- [AWS in Plain English](https://www.expeditedssl.com/aws-in-plain-english)

#### Core concepts
- [7 things that new programmers should learn](https://codeaddiction.net/articles/43/7-things-that-new-programmers-should-learn)
- [Codd's 12 rules](https://www.w3resource.com/sql/sql-basic/codd-12-rule-relation.php)
- [Justice Will Take Us Millions Of Intricate Moves](https://www.crummy.com/writing/speaking/2008-QCon/)
- [Objects vs. Data Structures – Hacker Noon](https://hackernoon.com/objects-vs-data-structures-e380b962c1d2)
- [Hidden Costs of Memory Allocation](https://randomascii.wordpress.com/2014/12/10/hidden-costs-of-memory-allocation/)
- [What ORMs have taught me: just learn SQL](https://wozniak.ca/blog/2014/08/03/What-ORMs-have-taught-me-just-learn-SQL/index.html)
- [Stop Designing Languages. Write Libraries Instead.](http://lbstanza.org/purpose_of_programming_languages.html)
- [Mocking is not practical — Use fakes](https://medium.com/@june.pravin/mocking-is-not-practical-use-fakes-e30cc6eaaf4e)
- [Don’t Use Boolean Arguments, Use Enums](https://medium.com/better-programming/dont-use-boolean-arguments-use-enums-c7cd7ab1876a)

#### Databases
- [SQLBolt - Learn SQL with simple, interactive exercises.](https://sqlbolt.com/lesson/introduction)
- [modern SQL - A lost has changed since SQL-92](https://modern-sql.com/)

#### Git
- [Git from the Bottom Up](https://jwiegley.github.io/git-from-the-bottom-up/)
- [How to manage your Git history: Tips for keeping your commits tidy](https://blog.ubuntu.com/2018/12/12/tricks-for-keeping-a-tidy-git-commit-history)
- [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/)
- [The Communicative Value of Using Git Well](https://jeremykun.com/2020/01/14/the-communicative-value-of-using-git-well/)

#### JVM (Java Virtual Machine)
- [5 Java concepts explained: Overloading, overriding, shadowing, hiding, and obscuring](https://programming.guide/java/overloading-overriding-shadowing-hiding-obscuring.html)
- [The Trouble with Checked Exceptions](https://www.artima.com/intv/handcuffs.html)
- [Java Design Patterns](https://java-design-patterns.com/)
- [A guide to logging in Java](https://www.marcobehler.com/guides/a-guide-to-logging-in-java)
- [Visualizing Garbage Collection Algorithms](https://spin.atomicobject.com/2014/09/03/visualizing-garbage-collection-algorithms/)
- [How I once saved half a million dollars with a single character code change](https://blog.pitest.org/how-i-once-saved-half-a-million-dollars-with-a-single-character-code-change/)
- [Tricks with 'var' and anonymous classes (that you should never use at work)](https://blog.codefx.org/java/tricks-var-anonymous-classes/)
- [Understanding How Graal Works - a Java JIT Compiler Written in Java](https://chrisseaton.com/rubytruffle/jokerconf17/)
- [A categorized list of all Java and JVM features since JDK 8 to 14](https://advancedweb.hu/a-categorized-list-of-all-java-and-jvm-features-since-jdk-8-to-14/)
- [Experimenting with “mutation testing” and Kotlin](https://medium.com/@s4n1ty/experimenting-with-mutation-testing-and-kotlin-b515d77e85b5)
- [Mastering Kotlin standard functions: run, with, let, also and apply](https://medium.com/@elye.project/mastering-kotlin-standard-functions-run-with-let-also-and-apply-9cd334b0ef84)
- [GC progress from JDK 8 to JDK 17](https://kstefanj.github.io/2021/11/24/gc-progress-8-17.html)
- [Understanding the constant pool inside a Java class file](https://blogs.oracle.com/javamagazine/post/java-class-file-constant-pool)

#### \*nix
- [htop explained](https://peteris.rocks/blog/htop/)
- [A Great Vim Cheat Sheet](https://vimsheet.com/)
- [Things About Vim I Wish I Knew Earlier](https://blog.petrzemek.net/2016/04/06/things-about-vim-i-wish-i-knew-earlier/)
- [Vim Substitute Tricks](https://nikodoko.com/posts/vim-substitute-tricks/)
- [Ranges in Vim](https://nikodoko.com/posts/vim-ranges/#a-short-history-lesson)

#### Networking
- [A fun and colorful explanation of how DNS works](https://howdns.works/)
- [WebSockets vs. HTTP](https://ably.com/topic/websockets-vs-http)

#### Other platforms
- [Our journey to type checking 4 million lines of Python](https://dropbox.tech/application/our-journey-to-type-checking-4-million-lines-of-python)
- [What Is TypeScript? Pros and Cons of TypeScript over JavaScript](https://stxnext.com/blog/2019/08/30/typescript-pros-cons-javascript)

#### Web development
- [So, What the Heck is GraphQL? - Bits and Pieces](https://blog.bitsrc.io/so-what-the-heck-is-graphql-49c27cb83754)
- [GraphQL vs. REST](https://medium.com/@fagnerbrack/the-real-difference-between-graphql-and-rest-e1c58b707f97)

#### SLC (Software Life Cycle)
- [How to Do Code Reviews Like a Human (Part One)](https://mtlynch.io/human-code-reviews-1/)
- [How to Do Code Reviews Like a Human (Part Two)](https://mtlynch.io/human-code-reviews-2/)
- [The Pull Request Paradox: Merge Faster by Promoting Your PR](https://linearb.io/blog/the-pull-request-paradox-merge-faster-by-promoting-your-pr/)

#### Testing
- [So you want to be a Tester?](https://filfreire.com/posts/be_a_tester)
- [Why do record/replay tests of web applications break?](https://blog.acolyer.org/2016/05/30/why-do-recordreplay-tests-of-web-applications-break/)
- [Software Testing Anti-patterns](http://blog.codepipes.com/testing/software-testing-antipatterns.html)
- [Is BDD Automation Actually Killing Your Project?](https://saucelabs.com/blog/is-bdd-automation-actually-killing-your-project)
- [Modern Best Practices for Testing in Java](https://phauer.com/2019/modern-best-practices-testing-java)

#### Misc
- [Why Don't More People Work As Programmers?](https://www.forbes.com/sites/quora/2014/10/31/why-dont-more-people-work-as-programmers/#50a856e455c0)
- [Conversations with a six-year-old on functional programming](https://byorgey.wordpress.com/2018/05/06/conversations-with-a-six-year-old-on-functional-programming/)
- [Things I wish someone had explained about functional programming](https://jrsinclair.com/articles/2019/what-i-wish-someone-had-explained-about-functional-programming/)
- [code != computer science](https://www.ultrasaurus.com/2013/12/code-computer-science/)
- [Engineers Don’t Want Clean Code](https://www.linkedin.com/pulse/engineers-dont-want-clean-code-simon-rigden/)
- [Things You Should Never Do, Part I (throw away code and start from scratch)](https://www.joelonsoftware.com/2000/04/06/things-you-should-never-do-part-i/)
- [Writing good code: how to reduce the cognitive load of your code](https://chrismm.com/blog/writing-good-code-reduce-the-cognitive-load/)
- [ripgrep is faster than {grep, ag, git grep, ucg, pt, sift}](https://blog.burntsushi.net/ripgrep/)

### How-to's

#### Linux
- [Restricting process CPU usage using nice, cpulimit, and cgroups](https://blog.scoutapp.com/articles/2014/11/04/restricting-process-cpu-usage-using-nice-cpulimit-and-cgroups)
- [US International keyboard layout](https://dry.sailingissues.com/us-international-keyboard-layout.html)


### Talks and conferences

#### Core concepts
- [Concurrency Is Not Parallelism](https://vimeo.com/49718712)
- [How computer memory works](https://youtu.be/p3q5zWCw8J4)
- [Interpreters and Compilers (Bits and Bytes, Episode 6)](https://youtu.be/_C5AHaS1mOA)
- [Anders Hejlsberg on Modern Compiler Construction](https://channel9.msdn.com/Blogs/Seth-Juarez/Anders-Hejlsberg-on-Modern-Compiler-Construction)

#### JVM (Java Virtual Machine)
- [4 JVM Web Frameworks in 40 Minutes](https://youtu.be/nyz0AVIOTkI)
- [Collections.compare:JDK, Eclipse, Guava, Apache](https://youtu.be/QwZF8xQHlxE)
- [Get a Taste of Lambdas and Get Addicted to Streams](https://youtu.be/1OpAgZvYXLQ)
- [The Cost of Kotlin Language Features](https://youtu.be/ExkNNsDn6Vg)
- [Venkat Subramaniam Explains Java Modules: Why and How](https://youtu.be/DItYExUOPeM)
- [Life After Java 8](https://youtu.be/eBuFzQeiGe0)
- [What's New in Java 19: The end of Kotlin?](https://youtu.be/te3OU9fxC8U)
- [From jUnit to Mutation-Testing](https://youtu.be/9yG1c9Crnbk)
- [Java & SQL, Stronger Together](https://modern-sql.com/blog/2020-05/java-and-sql-stronger-together)

#### Web development
- [Why reactive architecture will take over the world](https://youtu.be/0oovNxZnkAE)
- [A no nonsense GraphQL and REST comparison](https://youtu.be/vgm_uGmspMI)
- [API First development with OpenAPI - You should you practise it !?](https://youtu.be/F9iF3a1Z8Y8)

#### SLC (Software Life Cycle)
- [Kanban for Developers](https://youtu.be/xyhu_0HZr0E)

#### Misc
- [AT&T Archives: The UNIX Operating System](https://www.youtube.com/watch?v=tc4ROCJYbm0)
- [You Should Learn to Program: Christian Genco](https://youtu.be/xfBWk4nw440)
- [Top hacker shows us how it's done](https://youtu.be/hqKafI7Amd8)
- [Scouting for Intellect](https://youtu.be/EnQCYZ8Oz8Q)
- [Not Everyone Should Code](https://youtu.be/EFwa5Owp0-k)
- [Questions I'm asking in interviews](https://jvns.ca/blog/2013/12/30/questions-im-asking-in-interviews)
- [Why we at $FAMOUS_COMPANY Switched to $HYPED_TECHNOLOGY](https://saagarjha.com/blog/2020/05/10/why-we-at-famous-company-switched-to-hyped-technology)
