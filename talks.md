# Talks

Here, you can find links to the talks I've given throughout my career. I hope
for you to find them helpful or at every least entertaining.

## 2020

* [Modern Testing - what it is and how to be prepared for it](https://youtu.be/vXRPGHxVNGA) [Globant's Quality Summit 2020] (Spanish)

## 2019

* [Taking Advantage of Balin and Kotlin for Automating Tests of Web Applications](https://youtu.be/h1n0KtNAxH8) [Globant's Quality Summit 2019] (Spanish)

## 2012

* [DuckDuckGo: Un buscador web que protege tu privacidad](https://youtu.be/nPs96pv_zz8) [Refresh Maracaibo] (Spanish)
